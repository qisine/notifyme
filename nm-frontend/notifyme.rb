require "sinatra"
require "sinatra/reloader" if development?
require "active_record"

set :root, File.dirname(__FILE__)
set :public_folder, File.join(File.dirname(__FILE__), "app")
set :static, true

get '/' do
  posts = Post.where("new = 0").order("post_id DESC").limit(10).all
  erb :index, locals: {posts: posts}
end

post "/subscriptions" do
  req = JSON.parse(request.body.read)
  email = nil
  if req && (e = req["email"])
    email = Email.where("email = ?", e).first
    if email
      email.active = true
      email.save!
    else
      email = Email.create!({email: e, active: true})
    end
  end
  content_type :json
  email ? email.to_json : "{}"
end

delete "/subscriptions" do
  e = params["email"]
  if e
    email = Email.where("email = ?", e).first
    if email
      email.active = false
      email.save!
    end
  end
  content_type :json
  email ? email.to_json : "{}"
end

ActiveRecord::Base.establish_connection(
  adapter: "sqlite3",
  database: "../nm-scraper/db/#{production? ? "prod" : "dev" }.sqlite3",
  pool: 20,
  timeout: 5000
)

class Email < ActiveRecord::Base
end

class Post < ActiveRecord::Base
end
