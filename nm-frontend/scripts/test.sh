#!/bin/bash

BASE_DIR=$(pwd)

echo ""
echo "Starting Testacular Server (http://vojtajina.github.com/testacular)"
echo "-------------------------------------------------------------------"

testacular "$BASE_DIR"/config/testacular.conf.js $*
