'use strict';
function SubscriptionsCtrl($scope, $resource) {
  $scope.Subscription = $resource(
    "/subscriptions"
  );

  $scope.emailRgx = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i

  $scope.subscribe = function() {
    if($scope.subscribeForm.$invalid) return;
    var email = $scope.Subscription.save({email: $scope.subscribeForm.email}, function() {
      $("form[name=subscribeForm] .alert-success").show().fadeOut('slow');
    });
  }

  $scope.unsubscribe = function() {
    if($scope.unsubscribeForm.$invalid) return;
    var email = $scope.Subscription.remove({email: $scope.unsubscribeForm.email}, function() {
      if(email.email){
        $("form[name=unsubscribeForm] .alert-success").show().fadeOut('slow');
      } else {
        $("form[name=unsubscribeForm] .trx-error").show().fadeOut('slow');
      }
    });
  }
}

SubscriptionsCtrl.$inject = ["$scope", "$resource"];
