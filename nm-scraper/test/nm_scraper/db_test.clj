(ns nm-scraper.db-test
  (:use clojure.test
        nm-scraper.db))

(deftest selected-posts-by-body-should-match-keyword
  (testing "A post selected on body text should contain the keyword in the body"
    (let [patterns ["\\bStudie\\b", "Proband", "\\bUni\\b"]]
          (doseq [pattern patterns]
            (let [posts (select-posts-by-body pattern)]
              (doseq [post posts]
                (is (not (nil? (re-find (re-pattern pattern) (:body post)))))))))))

(deftest selected-emails-should-be-active
  (testing "All emails selected should be active"
    (is (every? #(= (:active %1) 1) (select-emails)))))