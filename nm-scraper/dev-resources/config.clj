{
 :email {:hostname "smtp.gmail.com"
         :smtp-port "465"
         :ssl true
         :from {:address "notifyme@probandenabo.ch"
                :name "Der Glückliche Proband"}
         :auth {:user-name "probandenabo@gmail.com"
                :passwd "F123rst!"}}

 :db {:classname "org.sqlite.JDBC"
      :subprotocol "sqlite"
      :subname (get (System/getenv) "NM_DB") ;"db/dev.sqlite3"
      :enable_load_extension "true"}
}