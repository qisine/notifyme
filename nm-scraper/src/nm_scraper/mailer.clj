(ns nm-scraper.mailer
  (:import org.apache.commons.mail.SimpleEmail)
  (:require [nm-scraper.helper :as hlp]
            [clojure.tools.logging :only (info error debug) :as lg]))

(def default-subject "Probanden-Alarm: %s")
(def default-message
  "%s\n\n%s\n\n\n========================================================\nDu erhältst diese Email, weil du ein Probandenabo hast. Um dich auszutragen, besuche http://probandenabo.ch/#unsubscribeForm")

(defn- send-email [email, subject, msg]
  (doto (SimpleEmail.)
    (.setHostName (:hostname hlp/email))
    (.setSslSmtpPort (:smtp-port hlp/email))
    (.setSSL (:ssl hlp/email))
    (.addTo email)
    (.setCharset "UTF-8")
    (.setFrom (:address (:from hlp/email)) (:name (:from hlp/email)))
    (.setSubject subject)
    (.setMsg msg)
    (.setAuthentication (:user-name (:auth hlp/email)) (:passwd (:auth hlp/email)))
    (.send)))

(defn send-notification [post, email]
  (lg/info "sending post " (:title post) "to address" email)
  (send-email email
              (format default-subject (:title post))
              (format default-message (:url post) (hlp/ellipsis-truncate (:body post) 100))))