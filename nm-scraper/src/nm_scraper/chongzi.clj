;;;the chongzi - in charge of crawling the website
(ns nm-scraper.chongzi
  (:require [nm-scraper.helper :as hlp]
            [nm-scraper.parser :as p]
            [clojure.tools.logging :only (info error debug) :as lg]))


(def base-url "http://www.marktplatz.uzh.ch/Biete/student-jobs")
(def nth-page-url "http://www.marktplatz.uzh.ch/Biete/student-jobs&page=%s")

(defn fetch [resource]
  "Fetch and parse resource"
  (p/html-resource resource))

(defn fetch-url [url]
  "Fetch and parse an arbitrary URL"
  (fetch (java.net.URL. url)))

(defn fetch-nth-page [nth]
  "Fetch the nth page of posts"
  (fetch-url (format nth-page-url nth)))

(defn has-more-pages? [page]
  (some #(= (:text %1) ">") (p/parse-navigation-links page)))

(defn get-post-urls [page]
  (p/parse-post-links page))

(defn get-post [url]
  (assoc (p/parse-post (fetch-url url)) :url url))

(defn get-posts [urls]
  (map get-post urls))

(defn start-fetching [url-filter-cb post-results-cb]
  (loop [n 1]
    (let [page (fetch-nth-page n)
          post-urls (map :url (get-post-urls page))]
      (if-let [filtered-urls (seq (url-filter-cb post-urls))]
        (do
          (lg/debug "hitting individual posts!")
          (post-results-cb (get-posts filtered-urls))
          (if (and (>= (count filtered-urls) (count post-urls)) (has-more-pages? page))
            (recur (inc n))))))))