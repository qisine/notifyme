(ns nm-scraper.helper
  (:require [clojure.java.io :as io]))

(def config (eval (read-string (slurp (io/resource "config.clj")))))

(def db (:db config))
(def email (:email  config))

(defn get-checksum [text]
  (org.apache.commons.codec.digest.DigestUtils/md5Hex (.getBytes text)))

(defn extract-post-id-from-url [url]
  "Extracts the post id from the post's URL"
  (last (last (re-seq #"(\d+)\.htm" url))))

(defn ellipsis-truncate [s len]
  (if (> (count s) len)
    (str (subs s 0 (dec len)) "...")
    s))