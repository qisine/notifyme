(ns nm-scraper.db
  (:require [clojure.java.jdbc :as dbc] [nm-scraper.helper :as hlp]))

(defn- prepare-select-sql [urls]
  "Builds up the SQL query for selecting posts by urls"
  (let [cnt (count urls)
        select "select * from posts"
        where "where url in (%s)"
        order-by "order by post_id desc"]
    (if (< cnt 1)
      [(str select " " order-by)]
      (vec (flatten [(format (str select " " where " " order-by)
                        (clojure.string/join "," (repeat cnt "?")))
                urls])))))

(defn select-posts
  "select all posts, optionally filtered by an vector of urls"
  ([] (select-posts []))
  ([urls]
     (dbc/with-connection hlp/db
       (dbc/with-query-results rs
         (prepare-select-sql urls)
         (doall rs)))))

(defn select-posts-by-body [pattern]
  "Select posts by body text"
  (dbc/with-connection hlp/db
    (dbc/with-query-results rs [ "SELECT load_extension('/usr/lib/sqlite3/pcre.so')"])
    (dbc/with-query-results rs ["select * from posts where new = 1 and body regexp ?" pattern] (doall rs))))

(defn select-emails []
  "Select all (active) emails"
  (dbc/with-connection hlp/db
    (dbc/with-query-results rs ["select * from emails where active=1 or active= 't'"] (doall rs))))

(defn mark-post-as-old [post]
  "Mark post as old, i.e. users have already been notified of this opportunity"
  (dbc/with-connection hlp/db
    (dbc/transaction
     (dbc/update-values :posts ["url = ?" (:url post)] {:new 0}))))

(defn- dbc-insert-post [record]
  "Insert posts, where record is a vector with the right column order"
  (dbc/with-connection hlp/db
    (dbc/transaction
     (dbc/insert-values :posts [:title, :body, :checksum, :url, :post_id] record))))
 
(defn insert-posts [records]
  "Insert posts"
  (dbc/with-connection hlp/db
    (dbc/transaction
     (doseq [r records] (dbc-insert-post (map #(%1 r) [:title, :body, :checksum, :url, :post_id]))))))

(defn insert-post [record]
  "Insert a single post"
  (dbc/with-connection hlp/db
    (dbc/transaction
     (dbc-insert-post (map #(%1 record) [:title, :body, :checksum, :url, :post_id])))))

(defn insert-email [email]
  "Insert an email address"
  (dbc/with-connection hlp/db
    (dbc/transaction
     (dbc/insert-values :emails [:email] [email]))))