(ns nm-scraper.notifier
  (:require [nm-scraper.mailer :as m]
            [nm-scraper.db :as db]
            [clojure.tools.logging :only (info error debug) :as lg]))

(def keywords ["\\bGruppendiskussion",
               "\\bProband",
               "\\bExperiment\\b",
               "\\bStudie\\b",
               "\\bEEG",
               "\\bMRI\\b",
               "\\bMRT\\b"])

(defn get-matching-posts []
  (let [posts-with-potential-dups (mapcat #(db/select-posts-by-body %1) keywords)
        post-ids (map :post_id posts-with-potential-dups)
        uniq-posts (zipmap post-ids posts-with-potential-dups)]
    (map last (seq uniq-posts))))

(defn notify-users []
  (doseq [post (get-matching-posts)]
    (do
      (lg/debug "found matching post to be notified:" (:title post))
      (doseq [email (db/select-emails)] (m/send-notification post (:email email)))
      (db/mark-post-as-old post))))
