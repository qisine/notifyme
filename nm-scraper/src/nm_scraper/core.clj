(ns nm-scraper.core
  (:gen-class)
  (:require [nm-scraper.db :as db]
            [nm-scraper.chongzi :as c]
            [nm-scraper.notifier :as nt]
            [nm-scraper.helper :as hlp]
            [clojure.tools.logging :only (info error debug) :as lg]))

(defn add-attrs [post]
  "Enrich post map with useful attributes"
  (assoc post
    :post_id (hlp/extract-post-id-from-url (:url post))
    :checksum (hlp/get-checksum (str (:title post) (:body post)))))

(defn filter-new-posts [fetched-posts-urls]
  (if-let [stored-posts-urls (seq (map :url (db/select-posts fetched-posts-urls)))]
    (filter #(not-any? (fn [u] (= %1 u)) stored-posts-urls) fetched-posts-urls)
    fetched-posts-urls))

(defn process-new-posts [posts]
  (db/insert-posts (map add-attrs posts)))

(defn get-working []
  "Main loop: first fetch all new posts, then notify users"
  (do (c/start-fetching filter-new-posts process-new-posts)
      (nt/notify-users)))

(defn -main []
  (loop []
    (lg/info "main loop")
    (try (get-working)
         (catch Exception ex (lg/error ex)))
    (Thread/sleep (* 60 1000))
    (recur)))