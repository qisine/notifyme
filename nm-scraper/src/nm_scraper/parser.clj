(ns nm-scraper.parser
  (:require [net.cgrand.enlive-html :as h]))

(defn html-resource [resource]
  "Fetch and parse an arbitrary URL"
  (h/html-resource resource))

(defn extract-url [node]
  (get-in node [:attrs :href]))

(defn parse-link [node]
  {:text (h/text node) :url (extract-url node)})

(defn parse-navigation-links [node]
  (map parse-link (h/select node [:.wp-pagenavi :a])))

(defn parse-post-links [node]
  (map parse-link (h/select node [[:div.post] :h2 :a])))

(defn parse-post [node]
  "Extract the post details from the given HTML node"
  (let [title (first (h/select node [:.content :h1]))
        body (first (h/select node [:.content :#content_main :.single_area :> h/first-child]))]
    {:title (h/text title) :body (h/text body)}))

(defn parse-posts [nodes]
  "Extract all posts from the current page"
  (map parse-post nodes))
